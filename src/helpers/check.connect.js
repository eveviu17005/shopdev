"use strict";

const mongoose = require("mongoose");
const os = require("os");

const _SECONDS = 5000;

const countConnect = () => {
  return mongoose.connections.length;
};

const checkOverload = () => {
  setInterval(() => {
    const numConnect = countConnect();
    const numCores = os.cpus().length;
    const memoryUsage = process.memoryUsage().rss / 1024 / 1024;

    // example maximun number of connections
    const maxConnect = numCores * 5;

    console.log(`Number of connections: ${numConnect}`);
    console.log(`Memory usage: ${memoryUsage} MB`);

    if (numConnect > maxConnect) {
      console.log("Overload connection detected");
    }
  }, _SECONDS); // Monitor every 5 seconds
};

module.exports = { countConnect, checkOverload };
